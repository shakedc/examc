import { Component, OnInit } from '@angular/core';

import { Router } from "@angular/router"

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {

  constructor(private router:Router) { }

  ngOnInit() {
  }
  toM() {
    this.router.navigate(['/main'])
  }
  toL() {
    this.router.navigate(['/login'])
  }
  toR() {
    this.router.navigate(['/register'])
  }
  toA() {
    this.router.navigate(['/about'])
  }

}
